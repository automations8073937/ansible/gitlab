<h3>GitLab Architecture</h3>
<h4>Frontend</h4>
- ELB: internal-gitlab-cluster-elb-1635014178.eu-west-1.elb.amazonaws.com<br>
- ew1-prd-gitlabfrontend01.mansion.gi<br>
- ew1-prd-gitlabfrontend01.mansion.gi<br>

<h4>Praefect</h4>
- ELB: internal-gitlab-praefect-elb-2492100.eu-west-1.elb.amazonaws.com:2305<br>
- ew1-prd-gitlabbackend04.mansion.gi<br>
- ew1-prd-gitlabbackend05.mansion.gi<br>
- ew1-prd-gitlabbackend06.mansion.gi<br>

<h4>Gitaly</h4>
- ew1-prd-gitlabbackend07.mansion.gi<br>
- ew1-prd-gitlabbackend08.mansion.gi<br>
- ew1-prd-gitlabbackend09.mansion.gi<br>

<h4>AWS RDS Postgres</h4>
- ew1-prd-gitlab20220513074750525300000001.cyplqvvywe4e.eu-west-1.rds.amazonaws.com

<h4>AWS ElastiCache Redis Replica Group</h4>
- master.gitlab-elasticache-01-prd-ew1-aws.vsbesg.euw1.cache.amazonaws.com

<h4>AWS S3 Bucket</h4>
- gitlab

<h4>Architecture Diagram</h4>
![GitLab Architecture Diagram](./Architecture.png "GitLab Architecture Diagram")

<h3>Ansible playbook to manage GitLab servers and Runners</h3>
Requirements:<br>
- Python 3.9 or above on Ansible host<br>
- Python 3 on Ansible clients<br>
- Python modules<br>
  - boto3<br>
  - jmeshpath<br>
- Ansible core v2.14.0 or above<br>
- Amazon AWS Ansible Galaxy installed (<code>ansible-galaxy collection install amazon.aws</code>)<br>
- SSH key with permission to clone/push GitLab deployment project over SSH<br>
- SSH key with permission access GitLab cluster hosts over SSH<br>
<br>
The latest stable GitLab version installed by the automation: 16.3.2-ee<br>
<br>
<h4>Configure GitLab</h4>
Goes through all the nodes one-by-one and applies their corresponding configuration, removing them from ELB when needed and restarts GitLab on them<br>
<code>ansible-playbook -i inventories/prd/ew1/ew1-prd-gitlabcluster/ew1-prd-gitlabcluster ConfigureGitLab.yml --limit="{group}"</code> <br>

<h4>Upgrade GitLab</h4>
Sets GitLab banner message, pushes StatusDashboard event, then runs through all the cluster nodes one-by-one removing them from ELB when needed and upgrades them. After the upgrade it runs the necessary post upgrade tasks. and functional tests<br>
By default, it runs the <a href="https://docs.gitlab.com/ee/update/zero_downtime.html" target="_blank">zero downtime upgrade</a>, if downtime is needed for the upgrade, the tag <i>maintenance_mode</i> must be set<br>
Requirements:<br>
- StatusDasboard event with <b>GitLab</b> service selected in <b>Planning</b> state<br>
- JIRA <b>DevOps</b> ticket created with the title following the regular expression '<b>GitLab Upgrade.*</b>' and in '<b>To Do</b>' state<br> 

<code>ansible-playbook -i inventories/prd/ew1/ew1-prd-gitlabcluster/ew1-prd-gitlabcluster UpgradeGitLab.yml</code><br>
Available tags:<br>
- maintenance_mode (If the upgrade does not support zero-downtime upgrade)

<h4>Install Runner</h4>
Installs GitLab runner on selected hosts. Don't run it without limit, or runner will be reinstalled on every runner<br>
<code>ansible-playbook -i inventories/prd/ew1/ew1-prd-gitlabcluster/ew1-prd-gitlabcluster InstallRunner.yml --limit="{hostname}"</code><br>

<h4>Configure Runner</h4>
Deploys GitLab Runner configuration on every runner host and restarts the application<br>
<code>ansible-playbook -i inventories/prd/ew1/ew1-prd-gitlabcluster/ew1-prd-gitlabcluster ConfigureRunner.yml</code><br>

<h4>Upgrade Runner</h4>
Upgrades GitLab Runner on every runner hosts and restarts the application<br>
<code>ansible-playbook -i inventories/prd/ew1/ew1-prd-gitlabcluster/ew1-prd-gitlabcluster UpgradeRunner.yml</code><br>

<h3>GitLab AWS Infrastructure IaC</h3>
<h4>AWS RDS Postgres Database (Managed by CI pipeline)</h4>
<a href="./IaC/gitlab_rds.tf" target="_blank">./IaC/gitlab_rds.tf</a>

<h4>AWS ElastiCacheRedis Replication Group (Managed by CI pipeline)</h4>
<a href="./IaC/gitlab_elasticache.tf" target="_blank">./IaC/gitlab_elasticache.tf</a>

<h4>AWS S3 Bucket (Managed by CI pipeline)</h4>
<a href="./IaC/s3_bucket.tf" target="_blank">./IaC/s3_bucket.tf</a>

<h4>EC2 Instances</h4>
EC2 instances are managed in the EC2 GitLab project:<br>
