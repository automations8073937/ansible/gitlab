resource "aws_elasticache_subnet_group" "redis_cluster_subnet_group" {
  name       = "gitlab-redis-cluster-subnet-group"
  subnet_ids = ["subnet-09c7596b4283306dd", "subnet-078ec695168195a41", "subnet-0cd5810d12f28486c"] #backend
}

resource "aws_elasticache_replication_group" "gitlab_elasticache_replication_group" {
  automatic_failover_enabled = true
  multi_az_enabled           = true
  transit_encryption_enabled = false
  availability_zones         = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
  replication_group_id       = "gitlab-elasticache-replication-group"
  description                = "gitlab elasticache replication group"
  node_type                  = "cache.t4g.small"
  parameter_group_name       = "default.redis6.x" #not enabled Cluster Mode
  num_cache_clusters         = 3
  port                       = 6379
  subnet_group_name          = aws_elasticache_subnet_group.redis_cluster_subnet_group.name
  security_group_ids         = ["sg-0383e72b7bafc48bc"] #gitlab-internal sg
  apply_immediately          = false
  auto_minor_version_upgrade = true
  maintenance_window         = "thu:09:00-thu:15:00"
}

####DNS AND ALARMS 
#https://github.com/cloudposse/terraform-aws-elasticache-redis/blob/master/main.tf
