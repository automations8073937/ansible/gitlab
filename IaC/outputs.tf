output "address" {
  value = module.ew1-prd-gitlab-rds.address
}

output "port" {
  value = module.ew1-prd-gitlab-rds.port
}