module "ew1-prd-gitlab-rds" {
  source = "gitlab.com/tj-terraform-modules/aws-db-instance/cloud"

  db_subnet_group_name   = "prd-db-eu-west-1"
  engine                 = "postgres"
  identifier_prefix      = "ew1-prd-gitlab"
  instance_class         = "db.t4g.medium"
  multi_az               = true
  password               = ""
  environment            = "PRD"
  service                = "gitlab"
  username               = "root"
  vpc_security_group_ids = ["sg-0383e72b7bafc48bc", "sg-06c36894206212f48", "sg-0fba82ab760b24da6"]
  db_name                = ""
}

module "praefect-database" {
  source = "gitlab.com/tj-terraform-modules/postgresql-database/service"

  name = ""

  depends_on = [module.ew1-prd-gitlab-rds]
}

module "gitlab-role" {
  source = "gitlab.com/tj-terraform-modules/postgresql-role/service"

  name     = ""
  password = ""
  login    = true

  database   = module.ew1-prd-gitlab-rds.db_name
  privileges = ["CONNECT", "CREATE", "TEMPORARY"]

  depends_on = [module.praefect-database]
}

module "praefect-role" {
  source = "gitlab.com/tj-terraform-modules/postgresql-role/service"

  name     = ""
  password = ""
  login    = true

  database   = module.praefect-database.name
  privileges = ["CONNECT", "CREATE", "TEMPORARY"]

  depends_on = [module.praefect-database]
}