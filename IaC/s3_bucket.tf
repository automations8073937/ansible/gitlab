module "gitlab_s3_bucket_origin" {
  source = "gitlab.com/tj-terraform-modules/aws-s3-bucket/cloud"

  bucket_name = "gitlab"
  environment = "PRD"
  service     = "gitlab"
  versioning  = "Suspended"
}
