provider "aws" {
  region = "eu-west-1"
}

terraform {
  required_providers {
    postgresql = {
      source = "cyrilgdn/postgresql"
    }
  }
}

provider "postgresql" {
  host            = module.ew1-prd-gitlab-rds.address
  port            = module.ew1-prd-gitlab-rds.port
  username        = module.ew1-prd-gitlab-rds.username
  password        = ""
  connect_timeout = 15
  superuser       = false
}
